/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : SportsCentre

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 19/04/2022 20:13:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_action
-- ----------------------------
DROP TABLE IF EXISTS `tb_action`;
CREATE TABLE `tb_action` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动名称',
  `action_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动地址',
  `action_price` decimal(10,2) DEFAULT NULL COMMENT '活动价格',
  `action_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动时间',
  `action_desc` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动描述',
  `action_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动图片',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='活动';

-- ----------------------------
-- Records of tb_action
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_action_course
-- ----------------------------
DROP TABLE IF EXISTS `tb_action_course`;
CREATE TABLE `tb_action_course` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_id` int DEFAULT NULL COMMENT '活动ID',
  `course_id` int DEFAULT NULL COMMENT '课程ID',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='活动课程';

-- ----------------------------
-- Records of tb_action_course
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_action_facility
-- ----------------------------
DROP TABLE IF EXISTS `tb_action_facility`;
CREATE TABLE `tb_action_facility` (
  `id` int NOT NULL AUTO_INCREMENT,
  `facility_id` int DEFAULT NULL COMMENT '设备ID',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='活动设备';

-- ----------------------------
-- Records of tb_action_facility
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_action_venue
-- ----------------------------
DROP TABLE IF EXISTS `tb_action_venue`;
CREATE TABLE `tb_action_venue` (
  `id` int NOT NULL,
  `action_id` int DEFAULT NULL COMMENT '活动id',
  `venue_id` int DEFAULT NULL COMMENT '场地ID',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='活动场地';

-- ----------------------------
-- Records of tb_action_venue
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_bankcar
-- ----------------------------
DROP TABLE IF EXISTS `tb_bankcar`;
CREATE TABLE `tb_bankcar` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL COMMENT '用户ID',
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '银行卡名称',
  `bank_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '银行卡号',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of tb_bankcar
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_course
-- ----------------------------
DROP TABLE IF EXISTS `tb_course`;
CREATE TABLE `tb_course` (
  `id` int NOT NULL AUTO_INCREMENT,
  `course_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '课程名称',
  `course_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '课程描述',
  `course_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '课程时长',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='课程';

-- ----------------------------
-- Records of tb_course
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_discount
-- ----------------------------
DROP TABLE IF EXISTS `tb_discount`;
CREATE TABLE `tb_discount` (
  `id` int NOT NULL AUTO_INCREMENT,
  `discount_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '折扣码名称',
  `discount_price` decimal(10,2) DEFAULT NULL COMMENT '折扣金额',
  `discount_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '折扣描述',
  `discount_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '折扣编号',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='折扣码';

-- ----------------------------
-- Records of tb_discount
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_facility
-- ----------------------------
DROP TABLE IF EXISTS `tb_facility`;
CREATE TABLE `tb_facility` (
  `id` int NOT NULL AUTO_INCREMENT,
  `facility_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备名称',
  `facility_desc` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '设备描述',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='设备';

-- ----------------------------
-- Records of tb_facility
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order` (
  `id` int NOT NULL,
  `user_id` int DEFAULT NULL COMMENT '下单人',
  `order_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编号',
  `action_id` int DEFAULT NULL COMMENT '活动ID',
  `order_status` int DEFAULT NULL COMMENT '订单状态:0 已预定,1已付款,2已完成,3已失效',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='订单';

-- ----------------------------
-- Records of tb_order
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_user`;
CREATE TABLE `tb_sys_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户名',
  `login_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '登录名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码',
  `avatar_id` int DEFAULT '1' COMMENT '头像id',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '手机号',
  `valid_flag` int DEFAULT '1' COMMENT '1:启用，0:禁用',
  `nick_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '昵称',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `update_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='管理员';

-- ----------------------------
-- Records of tb_sys_user
-- ----------------------------
BEGIN;
INSERT INTO `tb_sys_user` VALUES (1, 'admin', NULL, 'lueSGJZetyySpUndWjMBEg==', 1, '', 1, '管理员', '2022-04-16 16:49:50', NULL, '2022-04-16 17:15:04', NULL);
COMMIT;

-- ----------------------------
-- Table structure for tb_token
-- ----------------------------
DROP TABLE IF EXISTS `tb_token`;
CREATE TABLE `tb_token` (
  `id` int NOT NULL AUTO_INCREMENT,
  `token` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `uid` int DEFAULT '0',
  `type` int DEFAULT '0' COMMENT '0:APP,1 系统',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `create_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_user` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of tb_token
-- ----------------------------
BEGIN;
INSERT INTO `tb_token` VALUES (1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ7XCJwYXNzd29yZFwiOlwibHVlU0dKWmV0eXlTcFVuZFdqTUJFZz09XCIsXCJhdmF0YXJJZFwiOjEsXCJwaG9uZVwiOlwiXCIsXCJ2YWxpZEZsYWdcIjoxLFwiY3JlYXRlVGltZVwiOjE2NTAxMjc3OTAwMDAsXCJuaWNrTmFtZVwiOlwi566h55CG5ZGYXCIsXCJ1cGRhdGVUaW1lXCI6MTY1MDEyOTMwNDAwMCxcImlkXCI6MSxcInVzZXJOYW1lXCI6XCJhZG1pblwifSIsImlzcyI6ImFtaXIiLCJleHAiOjE2NTI2OTUwNzcsImN1c3RvbUFycmF5IjpbMSwyLDNdLCJjdXN0b21TdHJpbmciOiJhbWlyIn0.NmoTYBBefZa-Y-uIjyL7xrx2B5KiZar4lumCV-blPQ4', 1, 0, '2022-04-16 08:53:16', 'background', '2022-04-16 09:57:57', 'background');
COMMIT;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '昵称',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址',
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(0) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_venue
-- ----------------------------
DROP TABLE IF EXISTS `tb_venue`;
CREATE TABLE `tb_venue` (
  `id` int NOT NULL AUTO_INCREMENT,
  `venue_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '场馆名称',
  `venue_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '场馆地址',
  `open_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '开放时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='场馆';

-- ----------------------------
-- Records of tb_venue
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_venue_facility
-- ----------------------------
DROP TABLE IF EXISTS `tb_venue_facility`;
CREATE TABLE `tb_venue_facility` (
  `id` int NOT NULL AUTO_INCREMENT,
  `venue_id` int DEFAULT NULL COMMENT '场馆id',
  `facility_id` int DEFAULT NULL COMMENT '设备ID',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(0) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='场馆设备关联';

-- ----------------------------
-- Records of tb_venue_facility
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_vip
-- ----------------------------
DROP TABLE IF EXISTS `tb_vip`;
CREATE TABLE `tb_vip` (
  `id` int NOT NULL AUTO_INCREMENT,
  `vip_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '会员名称',
  `vip_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '会员描述',
  `vip_price` decimal(10,2) DEFAULT NULL COMMENT '会员价格',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='会员';

-- ----------------------------
-- Records of tb_vip
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
