package com.example.admin.service.impl;

import com.example.admin.entity.TbTokenEntity;
import com.example.admin.mapper.TbTokenMapper;
import com.example.admin.service.TbTokenService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author amir
 * @since 2022-04-16
 */
@Service
public class TbTokenServiceImpl extends ServiceImpl<TbTokenMapper, TbTokenEntity> implements TbTokenService {

}
