package com.example.admin.config;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.admin.entity.TbTokenEntity;
import com.example.admin.mapper.TbTokenMapper;
import com.example.admin.service.ISysUserService;
import com.example.common.entity.AjaxResult;
import com.example.common.utils.JsonUtils;
import com.example.common.utils.TokenUtils;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static com.example.common.entity.RespStatus.UNAUTHORIZATION;


@Component
@AllArgsConstructor
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {
    //    @Resource
//    private RedisUtil redisUtil;
    @Resource
    TbTokenMapper tokenMapp;
    private final ISysUserService userService;
    public static final String USER_KEY = "uid";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        //设置跨域--开始
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        if (httpRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            setHeader(httpRequest, httpResponse);
            return true;
        }
        //设置跨域--结束
        //从header中获取token
        String token = request.getHeader("token");
        //如果header中不存在token，则从参数中获取token
        if (StringUtils.isBlank(token)) {
            token = request.getParameter("token");
        }
        ServletResponse res = (HttpServletResponse) response;

        if (Strings.isEmpty(token)) {

            writeJsonResponse(res, JsonUtils.toJsonString(AjaxResult.error(UNAUTHORIZATION)));
            return false;
        }
        if (!TokenUtils.verifyToken(token)) {
            writeJsonResponse(res, JsonUtils.toJsonString(AjaxResult.error(UNAUTHORIZATION)));
            return false;
        }

        TbTokenEntity tbToken = tokenMapp.selectOne(new LambdaQueryWrapper<TbTokenEntity>().eq(TbTokenEntity::getToken, token));
//        if (tbToken == null) {
//            writeJsonResponse(res, JsonUtils.toJsonString(AjaxResult.error(UNAUTHORIZATION)));
//            return false;
//        }

        request.setAttribute("uid", "1");
        return true;
    }

    /**
     * 为response设置header，实现跨域
     */
    private void setHeader(HttpServletRequest request, HttpServletResponse response) {
        //跨域的header设置
        response.setHeader("Access-control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Methods", request.getMethod());
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
        //防止乱码，适用于传输JSON数据
        response.setHeader("Content-Type", "application/json;charset=UTF-8");
    }

    private void writeJsonResponse(ServletResponse servletResponse, String s) {
        servletResponse.setCharacterEncoding("utf-8");
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        res.setHeader("Content-Type", "application/json;charset=utf-8");
        PrintWriter w = null;
        try {
            w = res.getWriter();
        } catch (IOException e) {
            e.printStackTrace();
        }
        w.print(s);
        w.flush();
    }
}
