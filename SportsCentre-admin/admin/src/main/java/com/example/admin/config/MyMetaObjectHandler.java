package com.example.admin.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.example.admin.entity.SysUserEntity;
import com.example.admin.utils.UserUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Date;

@Primary
@Component("defaultMetaObjectHandler")
@ConditionalOnWebApplication
@ConditionalOnClass(MetaObjectHandler.class)
@Slf4j
public class MyMetaObjectHandler implements MetaObjectHandler {
    private final String CREATE_TIME= "createTime";
    private final String CREATE_USER= "createUser";
    private final String UPDATE_TIME= "updateTime";
    private final String UPDATE_USER= "updateUser";
    @Override
    public void insertFill(MetaObject metaObject) {
        SysUserEntity currentUser = UserUtils.getCurrentUser();
        setFieldValByName(CREATE_TIME,new Date(),metaObject);
        setFieldValByName(UPDATE_TIME,new Date(),metaObject);
        if (currentUser == null){
            setFieldValByName(CREATE_USER,"background",metaObject);
            setFieldValByName(UPDATE_USER,"background",metaObject);
        }else {
            setFieldValByName(CREATE_USER,currentUser.getLoginName(),metaObject);
            setFieldValByName(UPDATE_USER,currentUser.getLoginName(),metaObject);
        }


    }

    @Override
    public void updateFill(MetaObject metaObject) {
        SysUserEntity currentUser = UserUtils.getCurrentUser();

        setFieldValByName(UPDATE_TIME,new Date(),metaObject);
       if (currentUser == null){
           setFieldValByName(UPDATE_USER,"background",metaObject);
       }else {
           setFieldValByName(UPDATE_USER,currentUser.getLoginName(),metaObject);
       }
    }
}
