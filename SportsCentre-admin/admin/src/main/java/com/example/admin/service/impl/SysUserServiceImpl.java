package com.example.admin.service.impl;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.common.utils.TokenUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.admin.entity.SysUserEntity;
import com.example.admin.dto.SysUserDTO;
import com.example.admin.mapper.SysUserMapper;
import com.example.admin.service.ISysUserService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import java.util.Collection;
import lombok.AllArgsConstructor;
/**
 * 管理员Service业务层处理
 *
 * @author wangchonghui
 * @date 2022-04-16
 */
@Service
@AllArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements ISysUserService {

    private final SysUserMapper iSysUserMapper;
    @Override
    public SysUserEntity queryById(Long id){
        return getById(id);
    }

    @Override
    public SysUserEntity queryByToken() {
        String uid = TokenUtils.getUserIdByToken(TokenUtils.getToken());
        return this.getById(uid);
    }

    @Override
    public IPage<SysUserEntity> queryPageList(SysUserDTO dto) {
        IPage<SysUserEntity>  page = iSysUserMapper.queryPageList(new Page<SysUserEntity>(dto.getPageNum(), dto.getPageSize()), dto);
        return page;
    }

    @Override
    public List<SysUserEntity> queryList(SysUserDTO dto){
        List<SysUserEntity> list = iSysUserMapper.queryList(dto);
        return list;
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(SysUserEntity entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
