package com.example.admin.controller;


import com.example.common.core.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author amir
 * @since 2022-04-16
 */
@RestController
@RequestMapping("/tb-token")
public class TbTokenController extends BaseController {

}
