package com.example.admin.entity;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import java.util.Date;

/**
 * 管理员编辑对象 tb_sys_user
 *
 * @author wangchonghui
 * @date 2022-04-16
 */
@Data
@ApiModel("管理员编辑对象" )
@TableName(value = "tb_sys_user")
public class SysUserEntity {
    private static final long serialVersionUID = 1L;
    /**  */
    @TableId(value = "id",type = IdType.AUTO)
    @ApiModelProperty("id")
    private Long id;

    /** 用户名 */
    @TableField(value = "user_name")
    @ApiModelProperty("用户名")
    private String userName;


    /** 登录名 */
    @TableField(value = "login_name")
    @ApiModelProperty("登录名")
    private String loginName;


    /** 密码 */
    @TableField(value = "password")
    @ApiModelProperty("密码")
    @JsonIgnore
    private String password;

    @TableField(exist = false)
    private String token;

    /** 头像id */
    @TableField(value = "avatar_id")
    @ApiModelProperty("头像id")
    @JsonIgnore
    private Long avatarId;


    /** 手机号 */
    @TableField(value = "phone")
    @ApiModelProperty("手机号")
    private String phone;


    /** 1:启用，0:禁用 */
    @TableField(value = "valid_flag")
    @ApiModelProperty("1:启用，0:禁用")
    private Long validFlag;


    /** 昵称 */
    @TableField(value = "nick_name")
    @ApiModelProperty("昵称")
    private String nickName;


    /** 创建时间 */
    @TableField(value = "create_time")
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


    /** 创建人 */
    @TableField(value = "create_user")
    @ApiModelProperty("创建人")
    private String createUser;


    /** 更新时间 */
    @TableField(value = "update_time")
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;


    /** 修改人 */
    @TableField(value = "update_user")
    @ApiModelProperty("修改人")
    private String updateUser;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
.       append("id" , getId())
.       append("userName" , getUserName())
.       append("loginName" , getLoginName())
.       append("password" , getPassword())
.       append("avatarId" , getAvatarId())
.       append("phone" , getPhone())
.       append("validFlag" , getValidFlag())
.       append("nickName" , getNickName())
.       append("createTime" , getCreateTime())
.       append("createUser" , getCreateUser())
.       append("updateTime" , getUpdateTime())
.       append("updateUser" , getUpdateUser())
        .toString();
    }
}
