package com.example.admin.mapper;
import com.example.admin.dto.BankcarDTO;
import com.example.admin.entity.BankcarEntity;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 * 银行卡Mapper接口
 *
 * @author wangchonghui
 * @date 2022-04-16
 */
@Mapper
public interface BankcarMapper extends BaseMapper<BankcarEntity> {

    IPage<BankcarEntity> queryPageList(Page<BankcarEntity> page, BankcarDTO dto);

    List<BankcarEntity> queryList(BankcarDTO dto);
}
