package com.example.admin.service;

import com.example.admin.entity.SysUserEntity;
import com.example.admin.dto.SysUserDTO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.Collection;
import java.util.List;

/**
 * 管理员Service接口
 *
 * @author wangchonghui
 * @date 2022-04-16
 */
public interface ISysUserService extends IService<SysUserEntity> {
	/**
	 * 查询单个
	 * @return
	 */
	SysUserEntity queryById(Long id);

	SysUserEntity  queryByToken();
	/**
	 * 分页查询列表
	 */
	IPage<SysUserEntity> queryPageList(SysUserDTO dto);

	/**
	* 查询列表
	*/
	List<SysUserEntity> queryList(SysUserDTO dto);
	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
