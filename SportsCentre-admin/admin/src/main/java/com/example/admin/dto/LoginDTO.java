package com.example.admin.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author: amir
 * @Date: 2022/4/16 12:25 上午
 */
@Data
public class LoginDTO {
    @NotBlank(message = "用户名不能为空")
    private String userName;
    @NotBlank(message = "密码不能为空")
    private String passWord;
    //登录类型 1 管理员 2 会员
    private int loginType;
}
