package com.example.admin.mapper;

import com.example.admin.entity.TbTokenEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author amir
 * @since 2022-04-16
 */
public interface TbTokenMapper extends BaseMapper<TbTokenEntity> {

}
