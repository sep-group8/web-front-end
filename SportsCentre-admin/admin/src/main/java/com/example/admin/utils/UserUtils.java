package com.example.admin.utils;

import com.alibaba.fastjson.JSON;
import com.example.admin.entity.SysUserEntity;
import com.example.common.utils.TokenUtils;
import org.springframework.lang.Nullable;

/**
 * 获取当前登录用户
 *
 * @author xiebo
 */
public class UserUtils {
    /**
     * @return 当前登录用户
     */
    @Nullable
    public static SysUserEntity getCurrentUser() {
        SysUserEntity loginUser = null;
        try {
            String token = TokenUtils.getToken();
            String userJson = TokenUtils.decodeToken(token);
            loginUser = JSON.parseObject(userJson, SysUserEntity.class);
            return loginUser;
        }catch (Exception e){
            SysUserEntity userDTO = new SysUserEntity();
            userDTO.setLoginName("background");
            return userDTO;
        }
    }
}
