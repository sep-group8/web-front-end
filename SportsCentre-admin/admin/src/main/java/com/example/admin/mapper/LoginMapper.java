package com.example.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.admin.entity.BankcarEntity;

/**
 * @Author: amir
 * @Date: 2022/4/16
 */
public interface LoginMapper extends BaseMapper<BankcarEntity> {
}
