package com.example.admin.service.impl;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.admin.entity.BankcarEntity;
import com.example.admin.dto.BankcarDTO;
import com.example.admin.mapper.BankcarMapper;
import com.example.admin.service.IBankcarService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;
import java.util.Collection;
import lombok.AllArgsConstructor;
/**
 * 银行卡Service业务层处理
 *
 * @author wangchonghui
 * @date 2022-04-16
 */
@Service
@AllArgsConstructor
public class BankcarServiceImpl extends ServiceImpl<BankcarMapper, BankcarEntity> implements IBankcarService {

    private final BankcarMapper iBankcarMapper;
    @Override
    public BankcarEntity queryById(Long id){
        return getById(id);
    }

    @Override
    public IPage<BankcarEntity> queryPageList(BankcarDTO dto) {
        IPage<BankcarEntity>  page = iBankcarMapper.queryPageList(new Page<BankcarEntity>(dto.getPageNum(), dto.getPageSize()), dto);
        return page;
    }

    @Override
    public List<BankcarEntity> queryList(BankcarDTO dto){
        List<BankcarEntity> list = iBankcarMapper.queryList(dto);
        return list;
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(BankcarEntity entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
