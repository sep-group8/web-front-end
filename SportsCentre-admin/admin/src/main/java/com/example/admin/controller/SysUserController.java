package com.example.admin.controller;

import java.util.List;
import java.util.Arrays;

import com.example.common.annotation.Log;
import com.example.common.annotation.RepeatSubmit;
import com.example.common.core.BaseController;
import com.example.common.core.PageDataInfo;
import com.example.common.entity.AjaxResult;
import com.example.common.enums.BusinessType;
import io.swagger.annotations.ApiModelProperty;
import lombok.RequiredArgsConstructor;
import javax.swing.text.html.parser.Entity;
import javax.validation.constraints.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.example.admin.dto.SysUserDTO;
import com.example.admin.entity.SysUserEntity;
import com.example.admin.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 管理员Controller
 *
 * @author wangchonghui
 * @date 2022-04-16
 */
@Api(value = "管理员控制器", tags = {"管理员管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/admin/user")
public class SysUserController extends BaseController {

    private final ISysUserService iSysUserService;


    /**
    * 新增修改管理员数据
    * @param entity
    * @return
    */
    @ApiOperation("新增修改管理员")
    @Log(title = "管理员", businessType = BusinessType.INSERT)
    @RepeatSubmit
    @PostMapping("/saveOrUp")
    public AjaxResult<Void> saveOrUp(@Validated @RequestBody SysUserEntity entity){
        return iSysUserService.saveOrUpdate(entity)? AjaxResult.success():AjaxResult.error();
    }

    /**
    * 查询管理员列表
    * @param bo
    * @return
    */
    @ApiOperation("查询管理员列表")
    @GetMapping("/queryByPage")
    public AjaxResult<PageDataInfo> queryByPage(@Validated SysUserDTO bo) {
        return AjaxResult.success(getDataPage(iSysUserService.queryPageList(bo)));
    }

    /**
    * 导出管理员列表
    * @param dto
    * @return
    */
    @ApiOperation("导出管理员列表")
    @Log(title = "管理员", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult<SysUserEntity> export(@Validated SysUserDTO dto) {
        List<SysUserEntity> list = iSysUserService.queryList(dto);
        return AjaxResult.success(list);
    }

    /**
    * 获取管理员详细信息
    * @return
    */
    @ApiOperation("获取管理员详细信息")
    @GetMapping("/queryDetail/{id}")
    public AjaxResult<SysUserEntity> queryDetail(@NotNull(message = "主键不能为空")
                                                  @PathVariable("id") Long id) {
        return AjaxResult.success(iSysUserService.queryById(id));
    }

    @GetMapping("/queryByToken")
    @ApiModelProperty("根据Token 获取管理员信息")
    public AjaxResult<SysUserEntity> queryByToken(){
        return AjaxResult.success(iSysUserService.queryByToken());
    }
    /**
    * 删除管理员
    * @return
    */
    @ApiOperation("删除管理员")
    @Log(title = "管理员" , businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{ids}")
    public AjaxResult<Void> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] ids) {
        return toAjax(iSysUserService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
