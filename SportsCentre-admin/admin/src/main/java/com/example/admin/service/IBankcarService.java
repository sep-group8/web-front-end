package com.example.admin.service;

import com.example.admin.entity.BankcarEntity;
import com.example.admin.dto.BankcarDTO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.Collection;
import java.util.List;

/**
 * 银行卡Service接口
 *
 * @author wangchonghui
 * @date 2022-04-16
 */
public interface IBankcarService extends IService<BankcarEntity> {
	/**
	 * 查询单个
	 * @return
	 */
	BankcarEntity queryById(Long id);

	/**
	 * 分页查询列表
	 */
	IPage<BankcarEntity> queryPageList(BankcarDTO dto);

	/**
	* 查询列表
	*/
	List<BankcarEntity> queryList(BankcarDTO dto);
	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
