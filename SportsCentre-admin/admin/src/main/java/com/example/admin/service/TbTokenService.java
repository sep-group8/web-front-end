package com.example.admin.service;

import com.example.admin.entity.TbTokenEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author amir
 * @since 2022-04-16
 */
public interface TbTokenService extends IService<TbTokenEntity> {

}
