package com.example.admin.controller;

import com.example.admin.dto.LoginDTO;
import com.example.admin.service.LoginService;
import com.example.common.entity.AjaxResult;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: amir
 * @Date: 2022/4/16
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/admin/auth")
public class LoginController {

    private final LoginService loginService;
    @PostMapping("/login")
    public AjaxResult login(@Validated @RequestBody LoginDTO loginDTO){

       return loginService.login(loginDTO);
    }
    @PostMapping("/loginOut")
    public AjaxResult loginOut(HttpServletRequest request){

        return loginService.loginOut(request);
    }
}
