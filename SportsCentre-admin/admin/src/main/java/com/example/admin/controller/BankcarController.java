package com.example.admin.controller;

import java.util.List;
import java.util.Arrays;

import com.example.common.annotation.Log;
import com.example.common.annotation.RepeatSubmit;
import com.example.common.core.BaseController;
import com.example.common.core.PageDataInfo;
import com.example.common.entity.AjaxResult;
import com.example.common.enums.BusinessType;
import lombok.RequiredArgsConstructor;
import javax.swing.text.html.parser.Entity;
import javax.validation.constraints.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.example.admin.dto.BankcarDTO;
import com.example.admin.entity.BankcarEntity;
import com.example.admin.service.IBankcarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 银行卡Controller
 *
 * @author wangchonghui
 * @date 2022-04-16
 */
@Api(value = "银行卡控制器", tags = {"银行卡管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/admin/bankcar")
public class BankcarController extends BaseController {

    private final IBankcarService iBankcarService;


    /**
    * 新增修改银行卡数据
    * @param entity
    * @return
    */
    @ApiOperation("新增修改银行卡")
    @Log(title = "银行卡", businessType = BusinessType.INSERT)
    @RepeatSubmit
    @PostMapping("/saveOrUp")
    public AjaxResult<Void> saveOrUp(@Validated @RequestBody BankcarEntity entity){
        return iBankcarService.saveOrUpdate(entity)? AjaxResult.success():AjaxResult.error();
    }

    /**
    * 查询银行卡列表
    * @param bo
    * @return
    */
    @ApiOperation("查询银行卡列表")
    @GetMapping("/queryByPage")
    public AjaxResult<PageDataInfo> queryByPage(@Validated BankcarDTO bo) {
        return AjaxResult.success(getDataPage(iBankcarService.queryPageList(bo)));
    }

    /**
    * 导出银行卡列表
    * @param dto
    * @return
    */
    @ApiOperation("导出银行卡列表")
    @Log(title = "银行卡", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult<BankcarEntity> export(@Validated BankcarDTO dto) {
        List<BankcarEntity> list = iBankcarService.queryList(dto);
        return AjaxResult.success(list);
    }

    /**
    * 获取银行卡详细信息
    * @return
    */
    @ApiOperation("获取银行卡详细信息")
    @GetMapping("/queryDetail/{id}")
    public AjaxResult<BankcarEntity> queryDetail(@NotNull(message = "主键不能为空")
                                                  @PathVariable("id") Long id) {
        return AjaxResult.success(iBankcarService.queryById(id));
    }

    /**
    * 删除银行卡
    * @return
    */
    @ApiOperation("删除银行卡")
    @Log(title = "银行卡" , businessType = BusinessType.DELETE)
    @DeleteMapping("/delete/{ids}")
    public AjaxResult<Void> remove(@NotEmpty(message = "主键不能为空")
                                       @PathVariable Long[] ids) {
        return toAjax(iBankcarService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
