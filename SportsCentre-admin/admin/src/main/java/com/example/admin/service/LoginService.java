package com.example.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.admin.dto.LoginDTO;
import com.example.admin.entity.BankcarEntity;
import com.example.common.entity.AjaxResult;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: amir
 * @Date: 2022/4/16
 */
public interface LoginService extends IService<BankcarEntity> {

    AjaxResult login(LoginDTO loginDTO);

    AjaxResult loginOut(HttpServletRequest request);
}
