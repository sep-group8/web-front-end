package com.example.admin.mapper;
import com.example.admin.dto.SysUserDTO;
import com.example.admin.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 * 管理员Mapper接口
 *
 * @author wangchonghui
 * @date 2022-04-16
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUserEntity> {

    IPage<SysUserEntity> queryPageList(Page<SysUserEntity> page, SysUserDTO dto);

    List<SysUserEntity> queryList(SysUserDTO dto);
}
