package com.example.admin.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import java.util.Date;

/**
 * 【银行卡】编辑对象 tb_bankcar
 *
 * @author wangchonghui
 * @date 2022-04-16
 */
@Data
@ApiModel("【银行卡】编辑对象" )
@TableName(value = "tb_bankcar")
public class BankcarEntity {
    private static final long serialVersionUID = 1L;

    /**  */
    @TableId(value = "id",type = IdType.AUTO)
    @ApiModelProperty("")
    private Long id;


    /** 用户ID */
    @TableField(value = "user_id")
    @ApiModelProperty("用户ID")
    private Long userId;


    /** 银行卡名称 */
    @TableField(value = "bank_name")
    @ApiModelProperty("银行卡名称")
    private String bankName;


    /** 银行卡号 */
    @TableField(value = "bank_no")
    @ApiModelProperty("银行卡号")
    private String bankNo;


    /** 更新时间 */
    @TableField(value = "update_time")
    @ApiModelProperty("更新时间")
    private Date updateTime;


    /** 创建时间 */
    @TableField(value = "create_time")
    @ApiModelProperty("创建时间")
    private Date createTime;


    /** 创建人 */
    @TableField(value = "create_user")
    @ApiModelProperty("创建人")
    private String createUser;


    /** 更新人 */
    @TableField(value = "update_user")
    @ApiModelProperty("更新人")
    private String updateUser;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
.       append("id" , getId())
.       append("userId" , getUserId())
.       append("bankName" , getBankName())
.       append("bankNo" , getBankNo())
.       append("updateTime" , getUpdateTime())
.       append("createTime" , getCreateTime())
.       append("createUser" , getCreateUser())
.       append("updateUser" , getUpdateUser())
        .toString();
    }
}
