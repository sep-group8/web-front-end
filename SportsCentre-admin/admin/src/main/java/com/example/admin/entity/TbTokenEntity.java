package com.example.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import com.example.common.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import static com.baomidou.mybatisplus.annotation.FieldFill.INSERT;
import static com.baomidou.mybatisplus.annotation.FieldFill.INSERT_UPDATE;

/**
 * <p>
 *
 * </p>
 *
 * @author amir
 * @since 2022-04-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="TbToken对象", description="")
@TableName(value = "tb_token")
public class TbTokenEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String token;

    private Long uid;

    @ApiModelProperty(value = "0:APP,1 系统")
    private Integer type;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_user",fill = INSERT)
    public String createUser;
    @TableField(value = "update_user",fill = INSERT_UPDATE)
    @ApiModelProperty(value = "修改人")
    public String updateUser;
    @TableField(value = "create_time", fill = INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date createTime;
    @TableField(value = "update_time", fill = INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    public Date updateTime;
}
