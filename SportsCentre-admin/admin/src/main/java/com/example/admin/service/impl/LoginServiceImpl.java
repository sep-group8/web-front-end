package com.example.admin.service.impl;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.admin.dto.LoginDTO;
import com.example.admin.entity.BankcarEntity;
import com.example.admin.entity.SysUserEntity;
import com.example.admin.entity.TbTokenEntity;
import com.example.admin.mapper.BankcarMapper;
import com.example.admin.service.ISysUserService;
import com.example.admin.service.LoginService;
import com.example.admin.service.TbTokenService;
import com.example.common.entity.AjaxResult;
import com.example.common.utils.Fence;
import com.example.common.utils.TokenUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author: amir
 * @Date: 2022/4/16
 */
@Service
@Slf4j
@AllArgsConstructor
public class LoginServiceImpl extends ServiceImpl<BankcarMapper, BankcarEntity> implements LoginService {
   private final ISysUserService sysUserService;
   private final TbTokenService tokenService;
    @Override
    public AjaxResult login(LoginDTO loginDTO) {
        SysUserEntity sysUserEntity = sysUserService.getOne(new QueryWrapper<SysUserEntity>().lambda()
                .eq(SysUserEntity::getUserName, loginDTO.getUserName()));
        if (sysUserEntity == null){
            return AjaxResult.error("管理员不存在");
        }
        String encryption = Fence.encodeByMd5(loginDTO.getPassWord());
        if (!encryption.equals(sysUserEntity.getPassword())){

            return AjaxResult.error("密码错误");
        }
        JSONObject subjectJson = new JSONObject(sysUserEntity);
        String token = TokenUtils.createToken(subjectJson);
        sysUserEntity.setToken(token);
        TbTokenEntity tokenEntity = tokenService.getOne(new LambdaQueryWrapper<TbTokenEntity>()
                .eq(TbTokenEntity::getUid,sysUserEntity.getId()));
        if (tokenEntity == null){
            tokenEntity = new TbTokenEntity();
        }
        tokenEntity.setToken(token);
        tokenEntity.setUid(sysUserEntity.getId());
        tokenService.saveOrUpdate(tokenEntity);
        return AjaxResult.success(sysUserEntity);
    }

    @Override
    public AjaxResult loginOut(HttpServletRequest request) {
        boolean token = tokenService.remove(new LambdaQueryWrapper<TbTokenEntity>()
                .eq(TbTokenEntity::getToken, request.getHeader("token")));
        return token?AjaxResult.success(): AjaxResult.error();
    }
}
