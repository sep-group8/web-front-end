package com.example.common.entity;

public enum RespStatus {
    OK(200, "操作成功"),
    CREATEUP_SUCCESS(202,"更新成功"),
    CREATEUP_ERROR(203,"更新失败"),
    DELETE_ERROR(205,"删除失败"),
    ERROR(204, ""),
    UNAUTHORIZATION(209, "操作未授权"),
    CLIENT_ERROR(400, "请求异常，请检查参数"),
    SERVER_ERROR(500, "服务异常，稍后再试"),
    DB_ERROR(501, "数据库异常请重试");

    private Integer code;
    private String message;

    public Integer getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }


    private RespStatus(final Integer code, final String message) {
        this.code = code;
        this.message = message;
    }
}
