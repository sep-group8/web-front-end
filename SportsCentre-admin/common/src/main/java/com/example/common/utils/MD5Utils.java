package com.example.common.utils;

import org.apache.commons.codec.binary.Base64;
import sun.misc.BASE64Encoder;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Utils {

   /**
    * @Description: 对字符串进行md5加密
    */
   public static String getMD5Str(String strValue) throws Exception {
      MessageDigest md5 = MessageDigest.getInstance("MD5");
      String newstr = Base64.encodeBase64String(md5.digest(strValue.getBytes()));
      return newstr;
   }
   private static MessageDigest MD5 = null;

   static {
      try {
         MD5 = MessageDigest.getInstance("MD5");
      } catch (NoSuchAlgorithmException ex) {

      }
   }

   public static String encode(String value) {
      String result = "";
      if (value == null) {
         return result;
      }
      BASE64Encoder baseEncoder = new BASE64Encoder();
      try {
         result = baseEncoder.encode(MD5.digest(value.getBytes("utf-8")));
      } catch (Exception ex) {
      }
      return result;
   }
   public static void main(String[] args) {
      String userName = "st-web-wch363X";
      try {
         String md5 = getMD5Str("password01!");
         System.out.println(md5+"长度：===="+md5.length());
         System.out.println(encode("W.S0123456"+userName));
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
}
