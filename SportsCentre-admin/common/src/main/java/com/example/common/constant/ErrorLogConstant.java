package com.example.common.constant;

import lombok.Data;

/**
 * @Author: amir
 * @Date: 2021/8/23 5:29 下午
 */
@Data
public class ErrorLogConstant {

    public static String ErrorLogStatus_No = "0";//未解决
    public static String ErrorLogStatus_Yes = "1";//已解决
    public static String ErrorLogStatus_Skill = "2";//跳过
}
