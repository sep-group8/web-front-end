package com.example.common.constant;

/**
 * 操作类型
 */
public interface OperationTypeConstant {

    // 新增
    String OPERATION_TYPE_INSERT = "1";

    // 修改
    String OPERATION_TYPE_UPDATE = "2";


    String OPERATION_TYPE_INSERT_UPDATE = "3";

    // 删除
    String OPERATION_TYPE_DELETE = "4";
    //禁用
    String OPERATION_TYPE_ENABLE= "5";

    // 查询
    String OPERATION_TYPE_QUERY = "6";


}
