package com.example.common.utils;

import cn.hutool.core.text.UnicodeUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.DES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/*栅栏解密解密类*/
public class Fence {

    static String secretKey = "ucfccK1Mohw=";

    //info:需要加密的明文
    public static String encrypt(String info) {
        byte[] key = new byte[0];
        try {
            key = new BASE64Decoder().decodeBuffer(secretKey);
        } catch (IOException e) {
            e.printStackTrace();
        }
        DES des = SecureUtil.des(key);
        String encrypt = des.encryptHex(info);
        return encrypt;
    }

    //encrypt:需要解密的密文
    public static String decodeHutool(String encrypt) {
        byte[] key = new byte[0];
        try {
            key = new BASE64Decoder().decodeBuffer(secretKey);
        } catch (IOException e) {
            e.printStackTrace();
        }
        DES des = SecureUtil.des(key);
        return des.decryptStr(encrypt);
    }

    /*加密部分*/
    public static String encryption(String password) {
        String p = new String();
        String p1 = new String();
        String p2 = new String();
        for (int i = 0; i < password.length(); i++) {
            if (i % 2 == 0)
                p1 += p.valueOf(password.charAt(i));
            else
                p2 += p.valueOf(password.charAt(i));
        }
        return p1 + p2;
    }

    /*解密部分*/
    public static String decode(String FencePassword) {
        String password = new String();
        String p = new String();
        String p1 = FencePassword.substring(0, FencePassword.length() / 2);
        String p2 = FencePassword.substring(FencePassword.length() / 2);
        int i;
        for (i = 0; i < p1.length(); i++) {
            password += p.valueOf(p1.charAt(i)) + p.valueOf(p2.charAt(i));
        }
        if (FencePassword.length() % 2 != 0)
            password += p.valueOf(p2.charAt(i));
        return password;
    }
    public static String encodeByMd5(String string)  {

        // 确定计算方法
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
            Base64.Encoder base64Encoder = Base64.getEncoder();
            // 加密字符串
            return base64Encoder.encodeToString(md5.digest(string.getBytes("utf-8")));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return  "";
        }
    }
    public static void main(String[] args) {
        String s = UnicodeUtil.toUnicode("aaa123中文", true);
        String str = "aaa\\U4e2d\\u6587\\u111\\urtyu\\u0026";
        String res = UnicodeUtil.toString(str);
        String encryption = encodeByMd5("111111");
        String decode = encrypt("0Qz17Jw4XzoT67s6hEDQzg==");
        System.out.println(decode);
        System.out.println(encryption);
        //生成密钥，并转为字符串，可以储存起来，解密时可直接使用
        byte[] key = SecureUtil.generateKey(SymmetricAlgorithm.DES.getValue()).getEncoded();
        String secretKey = new BASE64Encoder().encodeBuffer(key);
        System.out.println(secretKey);
    }

}
