package com.example.common.annotation;

import org.apache.poi.ss.usermodel.CellType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: amir
 * @Date: 2021/11/17 3:58 下午
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface ExcelVOAttribte {
    /**
     * 导出到Excel中的名字、中文,用于表头
     * @return
     */
    public abstract String columnNameCN() default "";

    /**
     * 导出到Excel 中的名字，英文，用于表头
     * @return
     */
    public abstract String columnNameEN() default "";

    /**
     * 配置列的编号，对应1、2、3、4、5.....
     * @return
     */
    public abstract int columnNo() default 0;


    /**
     * 配置列的字段类型
     * @return
     */
    public abstract CellType columnType() default CellType.STRING;

    /**
     * 配置列的字段检验正则表达式
     * @return
     */
    public abstract String columnReg() default  "";

    /**
     * 配置列的字段校验正则校验不通过后的提示信息
     * @return
     */
    public abstract String columnRegMsg() default "";

    /**
     * 是否可为空
     * @return
     */
    public abstract boolean isNullable() default true;

    /**
     * 是否需要转成百分比
     * @return
     */
    public abstract boolean isPercent() default false;

    /**
     * 是否需要控制（包含：截取、添加）小数位
     * @return
     */
    public abstract boolean isTrans() default false;

    /**
     * 要控制的小数位数
     * @return
     */
    public abstract int precision() default 2;

    /**
     * 导出的NUMERIC格式的Exe是否需要千分位
     * @return
     */
    public abstract boolean needThousandPoint() default false;

    /**
     * 控制单元格内容的对齐方式
     * @return
     */
    public abstract String cellReside() default "CENTER";

    /**
     * 是否是行标题 仅当 isTrans= false时 才生效
     * @return
     */
    public abstract boolean isRowTitle() default false;

    /**
     * 当前列是否需要导出
     * @return
     */
    public abstract boolean isExport() default true;
}
