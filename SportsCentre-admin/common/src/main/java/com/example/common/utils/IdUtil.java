package com.example.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 在生成一千万个数据也没有出现重复，完全满足大部分需求。
 *
 */
public class IdUtil {

	private static String[] chars = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
			"o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8",
			"9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
			"U", "V", "W", "X", "Y", "Z" };

	/**生成无重复8位随机码
	 */
	public static String getShortUuid() {
		StringBuffer shortBuffer = new StringBuffer();
		String uuid = UUID.randomUUID().toString().replace("-", "");
		for (int i = 0; i < 8; i++) {
			String str = uuid.substring(i * 4, i * 4 + 4);
			int x = Integer.parseInt(str, 16);
			shortBuffer.append(chars[x % 0x3E]);
		}
		return shortBuffer.toString();

	}

	public static String getUuid() {
		StringBuffer shortBuffer = new StringBuffer();
		String uuid = UUID.randomUUID().toString().replace("-", "");
		return uuid;
	}

	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	private static final AtomicInteger atomicInteger = new AtomicInteger(1000000);

	/**
	 * 创建不连续的订单号
	 *
	 * @return 唯一的、不连续订单号
	 */
	public static synchronized String getOrderNoByUUID() {
		Integer uuidHashCode = UUID.randomUUID().toString().hashCode();
		if (uuidHashCode < 0) {
			uuidHashCode = uuidHashCode * (-1);
		}
		String date = simpleDateFormat.format(new Date());
		return date + uuidHashCode;
	}

	/**
	 * 获取同一秒钟 生成的订单号连续
	 *
	 * @return 同一秒内订单连续的编号
	 */
	public static synchronized String getOrderNoByAtomic() {
		atomicInteger.getAndIncrement();
		int i = atomicInteger.get();
		String date = simpleDateFormat.format(new Date());
		return date + i;
	}


}
