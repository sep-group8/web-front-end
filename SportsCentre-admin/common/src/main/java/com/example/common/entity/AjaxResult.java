package com.example.common.entity;

import com.example.common.utils.StringUtils;
import lombok.Data;

import java.io.Serializable;

@Data
public class AjaxResult<T> implements Serializable {

    private Integer code;
    private String msg;
    private T data;
    /**
     * 状态码
     */
    public static final String CODE_TAG = "code";

    /**
     * 返回内容
     */
    public static final String MSG_TAG = "msg";

    /**
     * 数据对象
     */
    public static final String DATA_TAG = "data";


    /**
     * 初始化一个新创建的 ResponseData 对象，使其表示一个空消息。
     */
    public AjaxResult() {
    }

    /**
     * 初始化一个新创建的 ResponseData 对象
     *
     * @param code 状态
     * @param msg  返回内容
     */
    public AjaxResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 初始化一个新创建的 ResponseData 对象
     *
     *  @param code 状态
     * @param msg  返回内容
     * @param data 数据对象
     */
    public AjaxResult(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;

        if (StringUtils.isNotNull(data)) {
          this.data = data;
        }
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static AjaxResult success() {
        return AjaxResult.success(RespStatus.OK);
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static AjaxResult success(Object data) {
        return AjaxResult.success("操作成功", data);
    }
/**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static AjaxResult success(RespStatus status) {
        return AjaxResult.success(status, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @return 成功消息
     */
    public static AjaxResult success(String msg) {
        return AjaxResult.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param status  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static AjaxResult success(RespStatus status, Object data) {
        return new AjaxResult(status.getCode(),status.getMessage(), data);
    }

     /**
     * 返回成功消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static AjaxResult success(String msg, Object data) {
        return new AjaxResult(RespStatus.OK.getCode(),msg, data);
    }



    /**
     * 返回错误消息
     *
     * @return
     */
    public static AjaxResult error() {
        return AjaxResult.error("操作失败");
    }

    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static AjaxResult error(String msg) {
        return AjaxResult.error(msg, null);
    }

    /**
     * 返回错误消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static AjaxResult error(String msg, Object data) {
        return new AjaxResult(RespStatus.ERROR.getCode(),msg, data);
    }

    /**
     * 返回错误消息
     *
     *
     * @param status 返回内容
     * @return 警告消息
     */
    public static AjaxResult error(RespStatus status)
    {
        return new AjaxResult(status.getCode(), null);
    }
}
