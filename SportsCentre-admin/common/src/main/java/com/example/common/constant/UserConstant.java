package com.example.common.constant;

/**
 * 用户常量字段
 *
 * @author xiebo
 */
public interface UserConstant {
    /**
     * 当前用户常量<br>
     * 从当前请求中获取当前用户信息
     */
    String CURRENT_USER = "RFS_LOGIN_USER";

    /**
     * 用户请求头<br>
     * 从当前请求头中解析用户
     */
    String USER_HEADER = "CURRENT_USER";
    /**
     * 放入session中的银行号
     */
    String BANK_CODE = "bankCode";
}
