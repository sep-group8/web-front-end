package com.example.common.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

/**
 * 邀请码生成器，算法原理：&lt;br/&gt;
 * 1) 获取id: 1127738 &lt;br/&gt;
 * 2) 使用自定义进制转为：gpm6 &lt;br/&gt;
 * 3) 转为字符串，并在后面加'o'字符：gpm6o &lt;br/&gt;
 * 4）在后面随机产生若干个随机数字字符：gpm6o7 &lt;br/&gt;
 * 转为自定义进制后就不会出现o这个字符，然后在后面加个'o',这样就能确定唯一性。最后在后面产生一些随机字符进行补全。&lt;br/&gt;
 */
public class ShareCodeUtil {
    private static final String[] storeInvitationChars={"a","c","b","d","f","e","h","i","j","k","l","m","n","o","p"
            ,"q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9"};
    /**
     * @author liushuaic
     * @date 2015/11/18 14:53
     * @desc  生成邀请码
     * 格式： 15（年）+01（月）+18（日）+id
     * */
    public static String generateInvitationCode(String userIdStr){
        Date date= Calendar.getInstance().getTime();
        SimpleDateFormat sdf2=new SimpleDateFormat("YYMMdd");
        String dateStr= sdf2.format(date);
        return String.valueOf(dateStr+userIdStr);
    }
    /**
     * @author liushuaic
     * @date 2015/11/26 18:01
     * @desc  生成邀请码
     * 格式： abcd12
     * */
    public static String generateInvitationCodeTwo(String userIdStr){

        int forSize=5-userIdStr.length();
        String randomStr="";
        for(int i=0;i<forSize;i++){
            Random random=new Random();
            int randomIndex=random.nextInt(35);
            randomStr=randomStr+storeInvitationChars[randomIndex];
        }
        return randomStr+userIdStr;
    }
    public static void main(String[] args) {
        //System.out.println(storeInvitationChars.length);

        String invitationCode=ShareCodeUtil.generateInvitationCode( "186121").toUpperCase();
        System.out.println(invitationCode);
    }
}
