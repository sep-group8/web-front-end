package com.example.common.constant;

/**
 * @Author: amir
 * @Date: 2022/3/15 1:56 上午
 */
public class Constants {
    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/profile";
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";
}
