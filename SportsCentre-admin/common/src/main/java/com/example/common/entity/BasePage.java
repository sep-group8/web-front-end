package com.example.common.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: amir
 * @Date: 2021/7/22 3:37 下午
 */
@Data
public class BasePage {

    @ApiModelProperty(value = "当前页码")
    Integer page = 1;
    @ApiModelProperty(value = "每页数量")
    Integer size = 10;
    @ApiModelProperty(value = "1:启用，0:禁用")
    private String validFlag;
}
