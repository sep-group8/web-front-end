package com.example.common.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysOperationLog {
    // 操作方法
    String operationMethod() default "";
    // 操作类型 1-新增 2-修改 3-删除
    String operationType() default "";
    // 操作描述
    String operationDesc() default "";
}
