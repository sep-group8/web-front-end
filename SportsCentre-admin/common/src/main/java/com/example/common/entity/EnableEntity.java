package com.example.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: amir
 * @Date: 2021/8/11 5:26 下午
 */
@Data
@ApiModel("启用禁用实体类")
public class EnableEntity extends BaseEntity{

    @ApiModelProperty("1启用，0禁用")
    private String enable;
    private Integer id;
}
