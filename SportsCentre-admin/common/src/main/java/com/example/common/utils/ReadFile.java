package com.example.common.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {
    static String  pathIn="C:\\Users\\Administrator\\Desktop\\橙一切图\\切图\\android\\drawable-xxxhdpi";//某个文件目录
    static String pathOut="D:\\SourceCode\\flutter\\orangenet\\lib\\assets\\images\\icon_a";//要复制到的地方
    /*
     * 读取指定路径下的文件名和目录名
     */
    public static void getFileList() {
        File file = new File(pathIn);
        File[] fileList = file.listFiles();
        for (int i = 0; i < fileList.length; i++) {
            if (fileList[i].isFile()) {
                String fileName = fileList[i].getName();
                copy(file.getAbsolutePath()+"\\"+fileName,pathOut+i+".png");
                System.out.println("文件：" + fileName);
            }
            if (fileList[i].isDirectory()) {
                String fileName = fileList[i].getName();
                System.out.println("目录：" + fileName);
            }
        }
    }

    private static void copy(String url1, String url2)  {
        FileInputStream in = null;
        try {
            in = new FileInputStream(new File(url1));
            FileOutputStream out = new FileOutputStream(new File(url2));
            byte[] buff = new byte[512];
            int n = 0;
            System.out.println("复制文件：" + "\n" + "源路径：" + url1 + "\n" + "目标路径：" + url2);
            while ((n = in.read(buff)) != -1) {
                out.write(buff, 0, n);
            }
            out.flush();
            in.close();
            out.close();
            System.out.println("复制完成");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        getFileList();
//        ReadFile rf = new ReadFile();
//        rf.getFileList();
//        String s = JSON.toJSONString(getNameList());
//        System.out.println(s);
    }




    private static  List<String> getNameList(){
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {

            list.add("22222222222222");
        }

        return list;
    }


}
