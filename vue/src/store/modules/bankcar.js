import {
    queryByPage,
    saveOrUp,
    exportExcel,
    deleteById
} from '@/api/bankcar'

const actions = {
    /** 新增修改 */
    saveOrUp({
                 commit
             }, data) {

        return new Promise((resolve, reject) => {
            saveOrUp(data).then(response => {
                const {
                    data
                } = response
                //
                resolve(data)
            }).catch(error => {
                reject(error)
            })
        })
    },
    /** 分页查询 */
    queryByPage({
                    commit
                }, queryFrom) {

        return new Promise((resolve, reject) => {
            queryByPage(queryFrom).then(response => {
                const {
                    data
                } = response
                //
                resolve(data)
            }).catch(error => {
                reject(error)
            })
        })
    },
    /** 导出 */
    exportExcel({commit}, queryFrom) {
        return new Promise((resolve, reject) => {
            exportExcel(queryFrom).then(response =>
            {
                const {
                    data
                } = response
                resolve(data)
            }).catch(erroe =>{
                reject(error)
            })
        })
    },
    /** 获取详情 */
    queryDetail({
                commit
                }, data){
        return new Promise((resolve,reject) =>{
            queryDetail(data).then(response =>{
                resolve(response)
            })
        })

    },
    /** 删除 批量删除 */
    deleteById({
               commit
               }, data) {
        return new Promise((resolve, reject) => {
            deleteById(data).then(response => {
                resolve(response)
            }).catch(error => {
                reject(error)
            })
        })
    }
}
export default {
    namespaced: true,
    actions
}
