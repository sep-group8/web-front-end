import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function queryByPage(query) {
  return request({
    url: '/admin/bankcar/queryByPage',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function queryDetail(id) {
  return request({
    url: '/admin/bankcar/queryDetail/' + id,
    method: 'get'
  })
}

// 新增修改【请填写功能名称】
export function saveOrUp(data) {
  return request({
    url: '/admin/bankcar/saveOrUp',
    method: 'post',
    data: data
  })
}


// 删除【请填写功能名称】
export function deleteById(data) {
  return request({
    url: '/admin/bankcar/delete/' + data,
    method: 'delete'
  })
}

// 导出【请填写功能名称】
export function exportExcel(query) {
  return request({
    url: '/admin/bankcar/export',
    method: 'get',
    params: query
  })
}
